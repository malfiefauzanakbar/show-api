<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Bootstrap 3 -->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Datatables CSS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    </head>
    <body>
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="dvContent"></div>
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Url</th>
                            <th>Source Url</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <!-- Datatables JS-->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#dvContent").append("<ul></ul>");
              var data_cats = [];
              $.ajax({
                type: "GET",
                url: "http://thecatapi.com/api/images/get?format=xml&results_per_page=20",
                dataType: "xml",
                success: function(xml){
                $(xml).find('image').each(function(){
                    var id = $(this).find('id').text();
                    var url = $(this).find('url').text();
                    var source_url = $(this).find('source_url').text();
                    data_cats.push([id, url, source_url]);
                });
                if ($.fn.dataTable.isDataTable('#example')) {
                    $('#example').dataTable().fnDestroy();
                }
                $('#example').DataTable({
                    data: data_cats,
                    autoWidth: false
                });
              },
              error: function() {
                alert("An error occurred while processing XML file.");
              }
              });
        });
    </script>
</html>
